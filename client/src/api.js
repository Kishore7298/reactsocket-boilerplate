import openSocket from 'socket.io-client';
const socket = openSocket('http://localhost:8000');

export function setTimeStamp(cb){
    socket.on('time',timestamp => cb(timestamp) );
    socket.emit('subscribeToTimer',1000);
}

