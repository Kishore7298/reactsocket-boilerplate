import React, { Component } from 'react';
import './App.css';
import { setTimeStamp } from './api';

class App extends Component {
  constructor(props){
    super(props);
    setTimeStamp((timestamp)=>{
      this.setState({timestamp})
    });
  }
  state={
    timestamp:"timestamp has not been set"
  }
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Our awesome drawing app</h2>
        </div>
        hello the timestamp is {this.state.timestamp}
      </div>
    );
  }
}

export default App;
