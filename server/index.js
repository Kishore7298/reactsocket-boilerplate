const io = require('socket.io')();

io.on('connection', (client)=>{
    client.on('subscribeToTimer',(interval)=>{
        console.log("client is subscribed to subscribeToTimer event");
        setInterval(()=>client.emit('time',new Date()), interval);
    });
});

const port = process.env.port || 8000;
io.listen(port);
console.log("Listening on port "+port);